public class APICHealthPolicyResponse {

	public class A2 {
		public String statusCode {get;set;} 
		public String detail {get;set;} 
		public String severity {get;set;} 
		public AdditionalStatus additionalStatus {get;set;} 
    }

	
	public class AccountStructure {
		public String code {get;set;} 
		public String name {get;set;} 
		public String type {get;set;} // in json: type
		public String level {get;set;} 
		// public AccountStructure accountStructure {get;set;} 
    }
	
	public class BenefitNetworkTier {
		public String benefitTierLevel {get;set;} 
		public String policyBenefitNetworkName {get;set;} 
		public BenefitNetwork benefitNetwork {get;set;} 
    }
	
	public class LegalEntity {
		public String organizationId {get;set;} 

	}
	
	public class Products {
		public String productType {get;set;} 
		// public ProductDestination productDestination {get;set;} 
	}
		
	public HealthPolicyResponse healthPolicyResponse {get;set;} 
	
	public class HealthProduct {
		public String isReferralRequired {get;set;} 
		public String pcpNeeded {get;set;} 
		public String productType {get;set;} 
		public String productName {get;set;} 
		public String productSubType {get;set;} 
		public String productBenefitAllowance {get;set;} 
	}
	
	public class FundingArrangement {
		public String fundingArrangementIndicator {get;set;} 
		public String fundingArrangementName {get;set;} 
	}
	
	public class ParentOrganization {
		public String typeCode {get;set;} 
		public String nameFull {get;set;} 
	}
	
	public class HealthPolicy {
		public List<Products> products {get;set;} 
		public String contractState {get;set;} 
		public String primaryPolicyType {get;set;} 
		public String policyName {get;set;} 
		public String policySubCategory {get;set;} 
		public String policyCategory {get;set;} 
		public EffectivePeriod policyEffectiveDate {get;set;} 
		public PolicyIdentifier policyIdentifier {get;set;} 
		public CarrierArrangement carrierArrangement {get;set;} 
		public List<BenefitNetworkTier> benefitNetworkTier {get;set;} 
		public PrimaryProduct primaryProduct {get;set;} 
		public List<OfferingLegalEntity> offeringLegalEntity {get;set;} 
		public List<AlternateIdentification> alternateIdentification {get;set;} 
		public String alternatePolicyName {get;set;} 
		public String lineOfBusinessName {get;set;} 
		public String lineOfBusinessCode {get;set;} 
		public String policySubCategoryName {get;set;} 
		public FundingArrangement fundingArrangement {get;set;} 
		public String policyId {get;set;} 
		public List<AccountStructure> accountStructure {get;set;} 
		public String subsegmentCode {get;set;} 
		public List<HealthProduct> healthProduct {get;set;} 
	}
	
	public class AdditionalStatus {
		public String detail {get;set;} 
		public String severity {get;set;} 
		public String statusCode {get;set;} 
		public String serviceName {get;set;} 
	}
	
	public class OfferingLegalEntity {
		public String networkType {get;set;} 
		public LegalEntity legalEntity {get;set;} 
	}
	
	public class PrimaryProduct {
		public String productId {get;set;} 
		public String productDistinctionCode {get;set;} 
	}
	
	public class CarrierArrangement {
		public String organizationId {get;set;} 
		public String nameFull {get;set;} 
		public String type_Z {get;set;} // in json: type
        public String type;
		public String typeCode {get;set;} 
		public ParentOrganization parentOrganization {get;set;} 
		public PolicyIdentifier organizationArrangementIdentifier {get;set;} 
		public String organizationArrangementID {get;set;} 
	}
	
	public class AlternateIdentification {
		public String code {get;set;} 
		public String valueShort {get;set;} 
	}
	
	public class BenefitNetwork {
		public String benefitNetworkId {get;set;} 
	}
	
	public class ReadHealthPolicyResponse {
		public HealthPolicy healthPolicy {get;set;} 
	}
	
	public class PolicyIdentifier {
		public String idSource {get;set;} 
		public String idValue {get;set;} 
		public String idType {get;set;} 
	}
	
	public class EffectivePeriod {
		public String datetimeBegin {get;set;} 
	}
	
	//public class ProductDestination {
	//	public String destinationName {get;set;} 
	//	public String destinationId {get;set;} 
	//	public EffectivePeriod effectivePeriod {get;set;} 
	//	public String sourceId {get;set;} 
	//}
	
	public class HealthPolicyResponse {
		public Status status {get;set;} 
		public ReadHealthPolicyResponse readHealthPolicyResponse {get;set;} 
	}
}