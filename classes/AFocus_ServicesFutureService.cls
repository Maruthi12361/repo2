public with sharing class AFocus_ServicesFutureService{
    
    public static List<Account> sendProviderEIEQuery (String providersRes) { 
                
        String responseJson;
		
             try {         
        	 responseJson = providersRes;
        	 System.debug('responseJson: '+responseJson);   
             responseJson = responseJson.replace('"number":', '"providerNumber":');
             System.debug('responseJson1: '+responseJson);   
             APICProviderResponse providerResponseObj = (APICProviderResponse) System.JSON.deserialize(responseJson, APICProviderResponse.class);
             System.debug('::providerResponseObj:::'+providerResponseObj);
            if(providerResponseObj != Null) {
                if(providerResponseObj.providersResponse.status.statusCode != '0') {
                    
                    throw new GenericAPICException('There was an error parsing the response from APIC');
                  
                }
                else {
                    if(providerResponseObj.providersResponse.status.additionalStatus[0].statusCode != '200') {
                        System.debug('providersResponse.status.additionalStatus[0].statusCode != 200');
                        throw new GenericAPICException(providerResponseObj.providersResponse.status.detail); 
                    }
                    else {
                        System.debug('providerResponseObj parseProviderJson');                       
                        return parseProviderJson(providerResponseObj);
                    }                   
                }
                
            }
            else {
					 throw new GenericAPICException('There was an error parsing the response from APIC');
            }
        }           
        catch(Exception e) {
            system.debug('EXCEPTION IN APICProvider.sendProviderEIEQuery()');
            system.debug('::::::: Message at 55 line'+ e.getMessage()+'::'+e.getLineNumber());
            system.debug(':::::: APICProvider'+ e.getStackTraceString());
        
           throw e;
        }
         
                        
    }
    
    public static List<Account> parseProviderJson (APICProviderResponse providerResponseObj) {
        List<Account> accounts = new List<Account>();
        for(APICProviderResponse.cls_providers provider : providerResponseObj.providersResponse.readProvidersResponse.providers) {
            Account account = new Account();
            accounts.add(account); 
            try {                   
                if(!String.isBlank(provider.PrimaryNPI.nationalProviderId)) {
                    account.NPI__c = provider.PrimaryNPI.nationalProviderId;
                }
            }
            catch(Exception e) {
                System.debug('Unable to get NPI: '+e.getMessage());
            }
                     
            account.CMM_External_Id__c = provider.providerID;
            account.Provider_Id__c = provider.providerID;
                     
            if(Boolean.valueOf(provider.isIndividualProvider)) {
                account.Provider_Last_Name__c = provider.individualProvider.nameLast;
                account.Provider_First_Name__c = provider.individualProvider.nameFirst;
                account.Provider_NPI_Type__c = 'Type 1';                
                
            }
            else {
                account.Facility_Name__c = provider.organizationProvider.nameFull;                
                account.Provider_NPI_Type__c = 'Type 2';                     
            }
                     
            account.Provider_Type__c = provider.providerType;
                
            try {               
                for(APICProviderResponse.cls_specialties primeSpeciality : provider.specialties) {
                    if(Boolean.valueOf(primeSpeciality.isPrimarySpecialty)) {
                        account.Primary_Specialty__c = primeSpeciality.specialty.specialtyName;
                        break;
                    }
                }
            }
            catch(Exception e) {
                System.debug('Unable to get Speciality: '+e.getMessage());
            }
                
            try {
                integer locationCount = 0;
               
                for(APICProviderResponse.cls_providerAtLocation providerLocation : provider.providerAtLocation) {
				       
			            if (locationCount>0) {
                        account = account.clone(true,false,true,true);
                        accounts.add(account);
                    }
                    locationCount++;
                        
                    try {
                        for(APICProviderResponse.cls_officeContacts officeContact : providerLocation.officeContacts) {
                            
                            try {
                                for(APICProviderResponse.cls_phonesVoice phoneVoice : officeContact.phonesVoice) {
                                    account.Phone = phoneVoice.providerNumber;
                                    if(!String.isBlank(phoneVoice.extension)) {
                                        account.Phone = account.Phone + ' ' + phoneVoice.extension;
                                    }
                                    break;  
                                }                                   
                            }
                            catch(Exception e) {
                                System.debug('Unable to get Phone Number: '+e.getMessage());
                            }                       
                        
                            try {
                                for(APICProviderResponse.cls_phonesFax phoneFax : officeContact.phonesFax) {
                                    account.Fax = phoneFax.providerNumber;
                                    break;
                                }                                   
                            }
                            catch(Exception e) {
                                System.debug('Unable to get Phone Fax: '+e.getMessage());
                            }
                                                                
                            try {
                                for(APICProviderResponse.cls_postalAddresses postalAddress : officeContact.postalAddresses) {
                                    account.BillingStreet = postalAddress.streetLine1;
                                    if(!String.isBlank(postalAddress.streetLine2)) {
                                        account.BillingStreet = account.BillingStreet + '\n' + postalAddress.streetLine2;
                                    }
                                    if(!String.isBlank(postalAddress.streetLine3)) {
                                        account.BillingStreet = account.BillingStreet + '\n' + postalAddress.streetLine3;
                                    }
                                    account.BillingPostalCode = postalAddress.postalCode;
                                    account.BillingState = postalAddress.state;
                                    account.BillingCity = postalAddress.city;                             
                                    break;  
                                }                                   
                            }
                            catch(Exception e) {
                                System.debug('Unable to get Postal Address: '+e.getMessage());
                            }                               
                        }
                    }
                    catch(Exception e) {
                        System.debug('Unable to get Office Contacts: '+e.getMessage());
                    }                       
                            
                    integer x=0; //Need a counter to keep track of if we're at the end
                    Set<String>tinSet = new Set<String>(); 
                        
                    try {
                        for(APICProviderResponse.cls_providerAtLocationTaxID providerTaxId : providerLocation.providerAtLocationTaxID) {
                            String tin = null;
                            String tinOwner = null; 
                            String type2Npi = null;
                            String npiType = null;                              
                                
                            try{
                                for(APICProviderResponse.cls_businessTaxEntity taxEntity : providerTaxId.businessTaxEntity) {
                                    tin = taxEntity.taxID;
                                    tinOwner = taxEntity.legalOwnerName; 
                                }                                       
                            }
                            catch(Exception e) {
                                System.debug('Unable to get Business Tax Entity: '+e.getMessage());
                            }                            
                                                        
                            try{
                                for(APICProviderResponse.NationalProviderIDClass npi : providerTaxId.nationalProviderID) {
                                    type2Npi =  npi.nationalProviderId;
                                    npiType = npi.nationalProviderIdType;
                                }                                       
                            }
                            catch(Exception e) {
                                System.debug('Unable to get NPI Type 2: '+e.getMessage());
                            } 
                            
                            if (x==0) { //First record, so we just populate the TIN and move on
                                tinSet.add(tin);
                                account.Tax_ID_Number__c = tin;
                                account.TIN_Owner_Name__c = tinOwner;
                                if(type2Npi != null && account.Provider_NPI_Type__c == 'Type 2' && npiType == '2') {
                                    account.NPI__c = type2Npi;
                                }                           
                            }
                            //System.debug('x value is : '+x + '; tinSet value : '+tinSet +' ; Tin Value : '+tin);
                            
                            if (x>0) {
                                //First check to see if the tax ID is already in our dataset
                                if (!tinSet.contains(tin)) {
                                    //No, it's not in there yet, so clone the account and add new account record to contain the value
                                    tinSet.add(tin);
                                    account = account.clone(true,false,true,true);
                                    accounts.add(account);
                                    account.Tax_ID_Number__c = tin;
                                    account.TIN_Owner_Name__c = tinOwner;
                                    if(type2Npi != null && account.Provider_NPI_Type__c == 'Type 2' && npiType == '2') {
                                        account.NPI__c = type2Npi;
                                    }                                
                                }
                            }
                            x++; 
                            System.debug('$$x:'+x);
                        }                       
                    }
                    catch(Exception e) {
                        System.debug('Unable to get providerAtLocationTaxID: '+e.getMessage());
                    }
                    Account parAccount = account;
                    if(providerLocation.providerNetworks.size()>0)
                    {
                        for(APICProviderResponse.cls_providerNetworks prvNtwk  : providerLocation.providerNetworks) 
                        {
                            if(prvNtwk.benefitNetworks.benefitNetworkId!='Aetna Direct NAP' && prvNtwk.benefitNetworks.benefitNetworkId!=null && prvNtwk.benefitNetworks.benefitNetworkId!='')
                            {
                                GPS_CTRL_ProviderSearchController.parAccountList.add(parAccount);
                                break;
                            }                        
                        }
                    }
                    
                    
                }
               
            }
            catch(Exception e) {
                System.debug('Unable to get providerAtLocation: '+e.getMessage());
            }                   
        }
        
        //ProviderSearchController.totalProviderRecords = 0;
        
        if(providerResponseObj.providersResponse.readProvidersResponse.paging != null && !String.isBlank(providerResponseObj.providersResponse.readProvidersResponse.paging.total)) {
            //ProviderSearchController.totalProviderRecords = Integer.valueOf(providerResponseObj.providersResponse.readProvidersResponse.paging.total);
            GPS_CTRL_ProviderSearchController.iPagingTotal=Integer.valueOf(providerResponseObj.providersResponse.readProvidersResponse.paging.total);
        }       
        
        return accounts;               
                 
    }
    
    public class GenericAPICException extends Exception {}
}