/**
 * 
 */
package com.rabit.cijob;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * @author sreenivas.m
 *
 */
@DynamoDBTable(tableName = "CIjobBuildProperties")
public class CIjobBuildProperties {
	
	private String orgName;
	private String key;
	private String projectName;
	private Integer buildNumber;
	private Map<String, String> buildPropsMap;
	private Map<String,String> deployPropsMap;
	/**
	 * @return the orgName
	 */
	@DynamoDBHashKey
	public String getOrgName() {
		return orgName;
	}
	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	/**
	 * @return the key
	 */
	@DynamoDBRangeKey
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key is combination of projectName or projectName_buildNumber to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the projectName
	 */
	@DynamoDBAttribute
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @return the buildNumber
	 */
	@DynamoDBAttribute
	public Integer getBuildNumber() {
		return buildNumber;
	}
	/**
	 * @param buildNumber the buildNumber to set
	 */
	public void setBuildNumber(Integer buildNumber) {
		this.buildNumber = buildNumber;
	}
	/**
	 * @return the buildPropsMap
	 */
	@DynamoDBAttribute
	public Map<String, String> getBuildPropsMap() {
		return buildPropsMap;
	}
	/**
	 * @param buildPropsMap the buildPropsMap to set
	 */
	public void setBuildPropsMap(Map<String, String> buildPropsMap) {
		this.buildPropsMap = buildPropsMap;
	}
	@DynamoDBAttribute
	public Map<String,String> getDeployPropsMap() {
		return deployPropsMap;
	}
	public void setDeployPropsMap(Map<String,String> deployPropsMap) {
		this.deployPropsMap = deployPropsMap;
	}
	
}
