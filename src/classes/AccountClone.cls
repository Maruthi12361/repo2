/**
 * AccountClone
 * @author: NO KNOWN AUTHOR FOR THIS CLASS
 */
public class AccountClone {
    public string AccountID{get;set;}
    public string myBrand {get;set;}
    public Account AccoutToClone {get;set;}
    
    public List<SelectOption> getBrands(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Royal','Royal'));
        options.add(new SelectOption('Celebrity','Celebrity'));
        options.add(new SelectOption('Azamara','Azamara'));
        return options;
    }
    
    public AccountClone(){
        if(ApexPages.currentPage().getParameters().get('Account')!=null && ApexPages.currentPage().getParameters().get('Account')!=''){
                AccountID=ApexPages.currentPage().getParameters().get('Account');
        }
    }
   
    /*public PageReference createAccount(){
        List<Schema.FieldSetMember> myfields =SObjectType.Account.FieldSets.AccountClone.getFields();
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : myFields) {
            query += f.getFieldPath() + ', ';
        }
        AccountID =  '\'' + AccountID+ '\'';  
        query += 'Id , Name FROM Account where ID = '+AccountID;  
        
        AccoutToClone = database.query(query);
        Account acctA = AccoutToClone.clone(false,true);  
        
        if(myBrand=='Azamara'){
            acctA.Name = acctA.Name+ ' Azamara';
            acctA.Brand__c = 'Azamara';
            acctA.Agency_Id__c = acctA.Agency_ID__c+'Z';
        }
        if(myBrand=='Royal'){
            acctA.Name = acctA.Name+ ' Royal';
            acctA.Brand__c = 'Royal';
            acctA.Agency_Id__c = acctA.Agency_ID__c+'R';
        }
        if(myBrand=='Celebrity'){
            acctA.Name = acctA.Name+ ' Celebrity';
            acctA.Brand__c = 'Celebrity';
            acctA.Agency_Id__c = acctA.Agency_ID__c+'C';
            
        }
        insert AcctA;
        
        PageReference PR = new PageReference ('/'+AcctA.Id) ;
        PR.setredirect(true);
        return PR;
    }*/   
}