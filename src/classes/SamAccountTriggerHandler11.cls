/*=========================================================================================
 * Date     : 10/21/2014
 * Developer    : Ken Gill
 * Purpose      : This class is the extension point of the Trigger Framework for adding Account trigger handlers.
 *                
 *     *******          
 *
 *=========================================================================================
 *                                 Update History
 *                                 ---------------
 * Date       Developer         Description
 *===========+============+================================================================
 * 11/18/14  |  KG    | Initial Version                                         
 *           |        | COE TEST LINE
 *             |        | COE TEST LINE -2
 *           | TPB    | COE TEST
 * 10/18/17  | TPB    | AR TEST API change
 * 11/01/2017| SAM    | Change
 * 11/29/2017| SAM    | Change
 * 11/29/2017| SAM    | Change 2
 * 01/09/2018| Tom    | AR Session Test - SAME LINE CHANGE
 * 02/18/2018| Tom    | AR test
 * 03/06/2018| Sam    | AR test
 * 04/18/2018| Tom    | AR DEMO
 * 5/27/2018|Alex| AR 4.2
 * TESTING
  * 05/28/2018| Alex    | AR Testing
  * 05/28/2018| Alex    | AR Testing 10:12 AM
  * 05/28/2018| Alex    | AR Testing 11:56 AM
  * 06/04/2018| Alex    | AR Testing 3:57 PM
  * 10/31/2018| Sam     | AR Hiddenbrooke
  * 10/31/2018| Sam     | AR Hiddenbrooke2
  
  AR Rollback POC1
  AR Change 12/27/2018
 *===========+============+================================================================
 */


public with sharing class SamAccountTriggerHandler11 {}